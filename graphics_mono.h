#ifndef _SSD1306_GRAPHICS_MONO_H_
#define _SSD1306_GRAPHICS_MONO_H_
#include <stdint.h>
#include "common.h"


enum ssd1306_errorMsg{	//Returned by certain functions
	ok=0,
	bitmap_InvalidSize,
	bitmap_DrawOffTheScreen,
	bitmap_FragmentStartsOffTheBitmap,
	bitmap_InvalidFragmentSize,
	circle_InvalidRadius,
	circle_RadiusModified,
};


/*
 *		D E C L A R A T I O N S
 * 		OF    F U N C T I O N S
 *
 *
 * * * * * * * * * * * * * * * * * * * * * */
void ssd1306_pixel(int16_t x, int16_t y, uint8_t color);
uint8_t ssd1306_getPixel(int16_t x, int16_t y);

void ssd1306_fill(uint8_t color);
void ssd1306_cls(void);


void ssd1306_rectangle(int16_t x, int16_t y, int16_t w, int16_t h, uint8_t color);
void ssd1306_rectangleFilled(int16_t x, int16_t y, int16_t w, int16_t h, uint8_t color);

void ssd1306_line(int16_t x0, int16_t y0, int16_t x1, int16_t y1, uint8_t color);
void ssd1306_lineDelta(int16_t x, int16_t y, int16_t dx, int16_t dy, uint8_t color);

void ssd1306_circle(int16_t x0, int16_t y0, int16_t r, uint8_t color);
void ssd1306_circleFilled(int16_t x0, int16_t y0, int16_t r, uint16_t color);

void ssd1306__roundedCorner(int16_t x0, int16_t y0, int16_t r, uint8_t cornername, uint8_t color);
void ssd1306__roundedCornerFilled(int16_t x0, int16_t y0, int16_t r, uint8_t cornername, int16_t delta, uint8_t color);

uint8_t ssd1306_rectangleRounded(int16_t x, int16_t y, int16_t w, int16_t h, int16_t r, uint8_t color);
uint8_t ssd1306_rectangleFilledRounded(int16_t x, int16_t y, int16_t w, int16_t h, int16_t r, uint8_t color);

void ssd1306_triangle(int16_t x0, int16_t y0, int16_t x1, int16_t y1, int16_t x2, int16_t y2, uint8_t color);
void ssd1306_triangleFilled(int16_t x0, int16_t y0, int16_t x1, int16_t y1, int16_t x2, int16_t y2, uint8_t color);

uint8_t ssd1306_bitmapFlash(int16_t x, int16_t y, const uint8_t *bitmap, int16_t bmpWidth, int16_t bmpHeight, uint8_t colorMode);
uint8_t ssd1306_bitmapFlashXBM(int16_t x, int16_t y, const uint8_t *bitmap, int16_t bmpWidth, int16_t bmpHeight, uint8_t colorMode);
uint8_t ssd1306_bitmapFlash_inWindow(int16_t x, int16_t y, int16_t windowOffsetX, int16_t windowOffsetY, int16_t windowWidth, int16_t windowHeight, uint8_t *bitmap, int16_t bmpWidth, int16_t bmpHeight, uint8_t colorMode);
uint8_t ssd1306_bitmapRam(int16_t x, int16_t y, uint8_t *bitmap, int16_t bmpWidth, int16_t bmpHeight, uint8_t colorMode);

uint8_t ssd1306_mirrorVerticalAxis(int16_t x, int16_t y, int16_t w, int16_t h);
uint8_t ssd1306_mirrorHorizontalAxis(int16_t x, int16_t y, int16_t w, int16_t h);


#endif /* _SSD1306_GRAPHICS_MONO_H_ */
