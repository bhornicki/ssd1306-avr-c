#ifndef _SSD1306_COMMON_H_
#define _SSD1306_COMMON_H_
#include <stdint.h>

/*
 * 		Config
 *			Also take a look inside ssd1306.h
 *
 * * * * * * * * * * * * * * * * * * * * * */
#define SSD1306_WIDTH	128
#define SSD1306_HEIGHT	64


#define EMULATOR_ENABLED 1
#define EMULATOR_PATH "../emulator/emulator.h"



/*
 * 		B A C K   E N D
 *
 *
 * * * * * * * * * * * * * * * * * * * * * */
#define SSD1306_frameBuff_size SSD1306_HEIGHT*SSD1306_WIDTH/8
extern uint8_t SSD1306_frameBuff[SSD1306_frameBuff_size];


#ifndef abs
	#define abs(x) ((x<0)?-x:x)
#endif
#ifndef sgn
	#define sgn(x) ((x<0)?-1:((x>0)?1:0))
#endif
#ifndef min
	#define min(a,b) (((a) < (b)) ? (a) : (b))
#endif



#if !defined(__AVR__)

	#warning No AVR selected, threating flash functions as normal pointers


	#ifndef pgm_read_byte
		#define pgm_read_byte(addr) (*(const unsigned char *)(addr))
	#endif
	#ifndef pgm_read_word
		#define pgm_read_word(addr) (*(const unsigned short *)(addr))
	#endif
	#ifndef pgm_read_dword
		#define pgm_read_dword(addr) (*(const unsigned long *)(addr))
	#endif
	#ifndef PSTR
		#define PSTR
	#endif
	#ifndef PROGMEM
		#define PROGMEM
	#endif

#endif

//Set pgm_read_pointer (used by fonts) to work with width predicted by architecture - useful with certain MCUs
#if !defined(__INT_MAX__) || (__INT_MAX__ > 0xFFFF)
	 #define pgm_read_pointer(addr) ((void *)pgm_read_dword(addr))
#else
	 #define pgm_read_pointer(addr) ((void *)pgm_read_word(addr))
#endif




#endif /* _SSD1306_COMMON_H_ */
