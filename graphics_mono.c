#if defined(__AVR__)
	#include <avr/pgmspace.h>
#endif
#include <stdint.h>
#include <string.h>
#include "common.h"
#include "graphics_mono.h"


/*
 * Set single pixel
 */
void ssd1306_pixel(int16_t x, int16_t y, uint8_t color) {
	//check if point is in screen range
	if ((x < 0) || (x >= SSD1306_WIDTH) || (y < 0) || (y >= SSD1306_HEIGHT)) return;

	switch (color) {
		case 0:
			*(SSD1306_frameBuff+x + (y >> 3) * SSD1306_WIDTH) &= ~(1 << (y % 8));	//erase color
			break;
		case 1:
			*(SSD1306_frameBuff+x + (y >> 3) * SSD1306_WIDTH) |= (1 << (y % 8));	//set color
			break;
		default:
			*(SSD1306_frameBuff+x + (y >> 3) * SSD1306_WIDTH) ^= (1 << (y % 8));	//toggle color
			break;
	}

}

/*
 * Chceck if pixel is filled
 */
uint8_t ssd1306_getPixel(int16_t x, int16_t y){
	if ((x < 0) || (x >= SSD1306_WIDTH) || (y < 0) || (y >= SSD1306_HEIGHT)) return 0;
	return *(SSD1306_frameBuff+(y>>3)*SSD1306_WIDTH+x)&(1<<(y%8))?1:0;
}

/*
 * Fill entire screen buffer
 */
void ssd1306_fill(uint8_t color) {
	if (color) color = 255;
	memset(SSD1306_frameBuff,color,SSD1306_frameBuff_size);
}
/*
 * Clear screen
 */
void ssd1306_cls(void)	{
	ssd1306_fill(0);
}
/*
 * Creates frame
 */
void ssd1306_rectangle(int16_t x, int16_t y, int16_t w, int16_t h, uint8_t color) {
	//Begin calculations from top-left corner
	if(w<0)	x-=(w=-w);
	if(h<0)	y-=(h=-h);

	//call faster version if filled rect will do everything
	if(w<3||h<3) {
		ssd1306_rectangleFilled(x,y,w,h,color);
		return;
	}

	ssd1306_rectangleFilled(x,y,w,1,color);					//top
	ssd1306_rectangleFilled(x,y+h-1,w,1,color);				//bottom
	ssd1306_rectangleFilled(x,(y+1),1,(h-2),color);			//left
	ssd1306_rectangleFilled((x+w-1),(y+1),1,(h-2),color);	//right

}


/*
 * Create straight lines and filled rectangles
 */
void ssd1306_rectangleFilled(int16_t x, int16_t y, int16_t w, int16_t h,
						   uint8_t color) {
	//Begin calculations from top-left corner
	if(w<0)	x-=(w=-w);
	if(h<0)	y-=(h=-h);

	//Ignore calculations off the screen. X dim
	//Left
	if (x < 0) {
		w += x;	//Remember x is negative
		x = 0;
	}
	//Right
	if ((x + w) >= SSD1306_WIDTH)
		w = (SSD1306_WIDTH - x);

	//Ignore calculations off the screen. Y dim
	//Above
	if (y < 0) {
		h += y;	//Remember y is negative
		y = 0;
	}
	//Under
	if ((y + h) > SSD1306_HEIGHT)
		h = (SSD1306_HEIGHT - y);

	//If there is nothing to draw
	if (w <= 0 || h <= 0) return;

	register uint8_t *pBuf = SSD1306_frameBuff;	//pointer to move around
	pBuf += ((y / 8) * SSD1306_WIDTH);			//Set page
	pBuf += x;									//Go to x in that page

	register uint8_t _w = w; //slightly faster

	if (h == 1) {	//Fast vertical

		register uint8_t mask = 1 << (y & 7);
		switch (color) {
			case 1:
				while (_w--) {
					*pBuf++ |= mask;
				}
				break;
			case 0:
				mask = ~mask;
				while (_w--) {
					*pBuf++ &= mask;
				}
				break;
			default:
				while (_w--) {
					*pBuf++ ^= mask;
				}
				break;
		}
	}
	else {

		register uint8_t _h = h;	//slightly faster
		register uint8_t _y = y;


	/*
	 * Do the part located in topmost page
	 */
		register uint8_t mod = (_y & 7);
		if (mod) {

			//Prepare mask
			mod = 8 - mod;
			//Lookup table
			static uint8_t premask[8] = { 0x00, 0x80, 0xC0, 0xE0, 0xF0, 0xF8, 0xFC, 0xFE };
			register uint8_t mask = premask[mod];

			// adjust the mask if we're not going to reach the end of this byte
			if (_h < mod) mask &= (0XFF >> (mod - _h));

			_w = w;
			while (_w--) {
				switch (color) {
					case 1:
						*(pBuf + _w) |= mask;
						break;
					case 0:
						*(pBuf + _w) &= ~mask;
						break;
					default:
						*(pBuf + _w) ^= mask;
						break;
				}
			}
			if (_h < mod) return;	//If our job is done - entire rectangle fits in one page

			_h -= mod;				//adjust h to height need-to-be-done
			pBuf += SSD1306_WIDTH;	//go to next page
		}

	/*
	 * Do the part located in pages between topmost && bottommost
	 */
		if (_h >= 8) {
			if (color >= 2) { //Slower version for toggle color
				do {
					_w = w;
					while (_w--) {
						*(pBuf + _w) = ~(*(pBuf+_w));
					}

					_h -= 8;				//adjust h to height need-to-be-done
					pBuf += SSD1306_WIDTH;	//go to next page
				} while (_h >= 8);
			}
			else {			//Slightly faster
				register uint8_t val = (color == 1) ? 255 : 0;

				do {
					_w = w;
					while (_w--) {
						*(pBuf + _w) = val;
					}

					_h -= 8;				//adjust h to height need-to-be-done
					pBuf += SSD1306_WIDTH;	//go to next page
				} while (_h >= 8);
			}
		}

	/*
	 * Do the part located in bottommost page
	 */
		if (_h) {
			//Prepare mask
			mod = _h & 7;
			//Lookup table
			static uint8_t postmask[8] = { 0x00, 0x01, 0x03, 0x07, 0x0F, 0x1F, 0x3F, 0x7F };
			register uint8_t mask = postmask[mod];


			_w = w;
			while (_w--) {
				switch (color) {
					case 1:
						*(pBuf + _w) |= mask;
						break;
					case 0:
						*(pBuf + _w) &= ~mask;
						break;
					default:
						*(pBuf + _w) ^= mask;
						break;
				}
			}
		}
	}
}





/*
 * Create sloped line based on start and end point
 */
void ssd1306_line(int16_t x0, int16_t y0, int16_t x1, int16_t y1, uint8_t color){
	ssd1306_lineDelta(x0,y0,x1-x0+1,y1-y0+1,color);
}

/*
 * Create sloped line
 */
void ssd1306_lineDelta(int16_t x, int16_t y, int16_t dx, int16_t dy, uint8_t color) {
	//Begin calculations from top-left corner
	if(dx<0)	x-=(dx=-dx);
	if(dy<0)	y-=(dy=-dy);

	--dx;		//TODO: Find why do I need 'em
	--dy;

	if(!dx){	//If there is no slope
		ssd1306_rectangleFilled(x,y,1,dy,color);
		return;
	}
	if(!dy){	//If there is no slope
		ssd1306_rectangleFilled(x,y,dx,1,color);
		return;
	}

	//Ignore calculations off the screen.
	if(((x<0)&&(x+dx<0))||								//left
		((x>SSD1306_WIDTH)&&(x+dx>SSD1306_WIDTH))||		//right
		((y<0)&&(y+dy<0))||								//top
		((y>SSD1306_HEIGHT)&&(y+dy>SSD1306_HEIGHT)))	//bottom
			return;


	int16_t i,sdx,sdy,dxabs,dyabs,x1,y1,px,py;
	dxabs=abs(dx);
	dyabs=abs(dy);
	sdx=sgn(dx);
	sdy=sgn(dy);
	x1=dyabs>>1;
	y1=dxabs>>1;
	px=x;
	py=y;

	ssd1306_pixel(px,py,color);
	if (dxabs>=dyabs) { /* the line is more horizontal than vertical */
		for(i=0;i<dxabs;i++) {
			y1+=dyabs;
			if (y1>=dxabs) {
				y1-=dxabs;
				py+=sdy;
			}
			px+=sdx;
			ssd1306_pixel(px,py,color);

		}
	}
	else { /* the line is more vertical than horizontal */
		for(i=0;i<dyabs;i++) {
			x1+=dxabs;
			if (x1>=dyabs) {
				x1-=dyabs;
				px+=sdx;
			}
			py+=sdy;
			ssd1306_pixel(px,py,color);
		}
	}
}

/*
 * Create circle with x0,y0 in the middle
 */
void ssd1306_circle(int16_t x0, int16_t y0, int16_t r, uint8_t color) {
	r=abs(r);
	if(r==0) return;

	int16_t f = 1 - r;
	int16_t ddF_x = 1;
	int16_t ddF_y = -2 * r;
	int16_t x = 0;
	int16_t y = r;

	ssd1306_pixel(x0  , y0+r, color);
	ssd1306_pixel(x0  , y0-r, color);
	ssd1306_pixel(x0+r, y0  , color);
	ssd1306_pixel(x0-r, y0  , color);

	while (x<y) {
		if (f >= 0) {
			y--;
			ddF_y += 2;
			f += ddF_y;
		}
		x++;
		ddF_x += 2;
		f += ddF_x;

		ssd1306_pixel(x0 + x, y0 + y, color);
		ssd1306_pixel(x0 - x, y0 + y, color);
		ssd1306_pixel(x0 + x, y0 - y, color);
		ssd1306_pixel(x0 - x, y0 - y, color);
		if(x!=y){	//Fix for toggling color
			ssd1306_pixel(x0 + y, y0 + x, color);
			ssd1306_pixel(x0 - y, y0 + x, color);
			ssd1306_pixel(x0 + y, y0 - x, color);
			ssd1306_pixel(x0 - y, y0 - x, color);
		}
	}
}

/*
 * Create filled circle with x0,y0 in the middle
 */
void ssd1306_circleFilled(int16_t x0, int16_t y0, int16_t r, uint16_t color) {
	r=abs(r);
	if(r==0) return;

	ssd1306_rectangleFilled(x0, y0-r,1, 2*r+1, color);
	ssd1306__roundedCornerFilled(x0, y0, r, 3, 0, color);
}

/*
 * Helper for creating circles, rounded corners
 *
 *	Cornername argument:
 *  1 | 2
 *  --X--
 *  8 | 4
 *	X is the starting point.
 */
void ssd1306__roundedCorner(int16_t x0, int16_t y0, int16_t r, uint8_t cornername, uint8_t color) {
	r=abs(r);
	if(r==0) return;

	int16_t f     = 1 - r;
	int16_t ddF_x = 1;
	int16_t ddF_y = -2 * r;
	int16_t x     = 0;
	int16_t y     = r;

	while (x<y-1) {
		if (f >= 0) {
			y--;
			ddF_y += 2;
			f     += ddF_y;
		}
		x++;
		ddF_x += 2;
		f     += ddF_x;
		if (cornername & 0x4) {
			ssd1306_pixel(x0 + x, y0 + y, color);
			ssd1306_pixel(x0 + y, y0 + x, color);
		}
		if (cornername & 0x2) {
			ssd1306_pixel(x0 + x, y0 - y, color);
			ssd1306_pixel(x0 + y, y0 - x, color);
		}
		if (cornername & 0x8) {
			ssd1306_pixel(x0 - y, y0 + x, color);
			ssd1306_pixel(x0 - x, y0 + y, color);
		}
		if (cornername & 0x1) {
			ssd1306_pixel(x0 - y, y0 - x, color);
			ssd1306_pixel(x0 - x, y0 - y, color);
		}
	}
}

/*	TODO: Check how does this function react to negative delta or r=0
 *
 * Helper for creating (filled) circles, rounded corners
 *
 *	Cornername argument:
 *	  |
 *	2 X 1
 *	  |
 *  X is the starting point.
 */
void ssd1306__roundedCornerFilled(int16_t x0, int16_t y0, int16_t r,uint8_t cornername, int16_t delta, uint8_t color) {
	r=abs(r);
	delta=abs(delta);

	int16_t f     = 1 - r;
	int16_t ddF_x = 1;
	int16_t ddF_y = -2 * r;
	int16_t x     = 0;
	int16_t y     = r;

	while (x<y-1) {
		if (f >= 0) {
			y--;
			ddF_y += 2;
			f     += ddF_y;
		}
		x++;
		ddF_x += 2;
		f     += ddF_x;

		if (cornername & 0x1) {
			ssd1306_rectangleFilled(x0+x, y0-y,1, 2*y+1+delta, color);
			ssd1306_rectangleFilled(x0+y, y0-x,1, 2*x+1+delta, color);
		}
		if (cornername & 0x2) {
			ssd1306_rectangleFilled(x0-x, y0-y,1, 2*y+1+delta, color);
			ssd1306_rectangleFilled(x0-y, y0-x,1, 2*x+1+delta, color);
		}
	}
}

/* TODO: Check how does this function react to r=0 or r<0 (without abs())
 *
 * Create rectangle with rounded corners
 * Returns errorMsg
 *
 * If radius is too big, will be decreased.
 *
 */
uint8_t ssd1306_rectangleRounded(int16_t x, int16_t y, int16_t w,int16_t h, int16_t r, uint8_t color) {
	r=abs(r);
	//Begin calculations from top-left corner
	if(w<0)	x-=(w=-w);
	if(h<0)	y-=(h=-h);

	//Check if the radius is acceptable
	if(r<0)	return circle_InvalidRadius;
	uint16_t rModified = min(w,h)*5/9;	// 5/9 was guesstimated
	if(r>rModified) {
		r=rModified;
		rModified=circle_RadiusModified;
	}
	else rModified=ok;

	ssd1306_rectangleFilled(x+r  , y    , w-2*r,1, color); // Top
	ssd1306_rectangleFilled(x+r  , y+h-1, w-2*r,1, color); // Bottom
	ssd1306_rectangleFilled(x    , y+r  ,1, h-2*r, color); // Left
	ssd1306_rectangleFilled(x+w-1, y+r  ,1, h-2*r, color); // Right

	ssd1306__roundedCorner(x+r    , y+r    , r, 1, color);
	ssd1306__roundedCorner(x+w-r-1, y+r    , r, 2, color);
	ssd1306__roundedCorner(x+w-r-1, y+h-r-1, r, 4, color);
	ssd1306__roundedCorner(x+r    , y+h-r-1, r, 8, color);
	return rModified;
}

/* TODO: Check how does this function react to r=0 or r<0 (without abs())
 *
 * Create filled rectangle with rounded corners
 * Returns errorMsg
 *
 * If radius is too big, will be decreased.
 *
 */
uint8_t ssd1306_rectangleFilledRounded(int16_t x, int16_t y, int16_t w, int16_t h, int16_t r, uint8_t color) {
	r=abs(r);
	//Begin calculations from top-left corner
	if(w<0)	x-=(w=-w);
	if(h<0)	y-=(h=-h);

	//Check if the radius is acceptable
	if(r<0)	return circle_InvalidRadius;
	uint16_t rModified = min(w,h)*5/9;	// 5/9 was guesstimated
	if(r>rModified) {
		r=rModified;
		rModified=circle_RadiusModified;
	}
	else rModified=ok;

	ssd1306_rectangleFilled(x+r, y, w-2*r, h, color);

	ssd1306__roundedCornerFilled(x+w-r-1, y+r, r, 1, h-2*r-1, color);
	ssd1306__roundedCornerFilled(x+r    , y+r, r, 2, h-2*r-1, color);
	return rModified;
}

/* TODO: toggle color via RAM bitmap
 *
 * Create triangle with given vertex'es
 */
void ssd1306_triangle(int16_t x0, int16_t y0,
					  int16_t x1, int16_t y1,
					  int16_t x2, int16_t y2,
						  uint8_t color) {

	ssd1306_line(x0, y0, x1, y1, color);
	ssd1306_line(x1, y1, x2, y2, color);
	ssd1306_line(x2, y2, x0, y0, color);
	if(color>1){	//SOMETIMES will fix toggle color
		ssd1306_pixel(x0,y0,color);
		ssd1306_pixel(x1,y1,color);
		ssd1306_pixel(x2,y2,color);
	}
}

/*
 * Create filled triangle with given vertex'es
 */
void ssd1306_triangleFilled(int16_t x0, int16_t y0,
							int16_t x1, int16_t y1,
							int16_t x2, int16_t y2,
								uint8_t color) {
	int16_t a, b, x, last,temp;

	// Sort coordinates by X order (x2 >= x1 >= x0)
	if (x0 > x1) {
		temp=y0;
		y0=y1;
		y1=temp;
		temp=x0;
		x0=x1;
		x1=temp;
	}
	if (x1 > x2) {
		temp=y2;
		y2=y1;
		y1=temp;
		temp=x2;
		x2=x1;
		x1=temp;
	}
	if (x0 > x1) {
		temp=y0;
		y0=y1;
		y1=temp;
		temp=x0;
		x0=x1;
		x1=temp;
	}

	if(x0 == x2) { // Handle awkward all-on-same-line case as its own thing
		a = b = y0;
		if(y1 < a)      a = y1;
		else if(x1 > b) b = y1;
		if(y2 < a)      a = y2;
		else if(y2 > b) b = y2;
		ssd1306_rectangleFilled(x0, a,1, b-a+1, color);
		return;
	}

	int16_t
			dx01 = x1 - x0,
			dy01 = y1 - y0,
			dx02 = x2 - x0,
			dy02 = y2 - y0,
			dx12 = x2 - x1,
			dy12 = y2 - y1;
	int32_t
			sa   = 0,
			sb   = 0;

	// For left part of triangle, find scanline crossings for segments
	// 0-1 and 0-2.  If x1=x2 (flat-left triangle), the scanline x1
	// is included here (and second loop will be skipped, avoiding a /0
	// error there), otherwise scanline x1 is skipped here and handled
	// in the second loop...which also avoids a /0 error here if x0=x1
	// (flat-right triangle).
	if(x1 == x2) last = x1;   // Include y1 scanline
	else         last = x1-1; // Skip it

	for(x=x0; x<=last; x++) {
		a   = y0 + sa / dx01;
		b   = y0 + sb / dx02;
		sa += dy01;
		sb += dy02;
		/* longhand:
			a = x0 + (x1 - x0) * (y - y0) / (y1 - y0);
			b = x0 + (x2 - x0) * (y - y0) / (y2 - y0);
			*/
		if(a > b){
			temp=a;
			a=b;
			b=temp;
		}
		ssd1306_rectangleFilled(x, a,1, b-a+1, color);
	}

	// For right part of triangle, find scanline crossings for segments
	// 0-2 and 1-2.  This loop is skipped if x1=x2.
	sa = dy12 * (x - x1);
	sb = dy02 * (x - x0);
	for(; x<=x2; x++) {
		a   = y1 + sa / dx12;
		b   = y0 + sb / dx02;
		sa += dy12;
		sb += dy02;
		/* longhand:
			a = x1 + (x2 - x1) * (y - y1) / (y2 - y1);
			b = x0 + (x2 - x0) * (y - y0) / (y2 - y0);
			*/
		if(a > b) {
			temp=a;
			a=b;
			b=temp;
		}
		ssd1306_rectangleFilled(x,a,1,  b-a+1, color);
	}
}







/*
 * Draws bitmap from FLASH.
 * Returns errorMsg
 *
 * Bytes are sorted in ROWs (not columns like frame buffer)	 *	Byte contain pixels in ASCENDING order
 *		row1: byte1 | byte2 |byte3							 *		0x01 - 1st pixel set (from left to right)
 *		row2: byte4 | byte5 |byte6							 *		0x02 - 2nd pixel set
 *		row3: ...											 *		0x80 - 8th pixel set
 *
 * ColorMode:
 * 0 - ERASEs bits masked by bitmap, blank space from bitmap is IGNORED
 * 1 - SETs bits masked by bitmap, blank space from bitmap is IGNORED
 * 2 - TOGGLEs bits masked by bitmap, blank space from bitmap is IGNORED
 *
 * 3 - OVERWRITE part of framebuffer with bitmap
 * 4 - OVERWRITE part of framebuffer with INVERTED bitmap
 * Other values are threated as 3
 */
uint8_t ssd1306_bitmapFlash(int16_t x, int16_t y, const uint8_t *bitmap, int16_t bmpWidth,
					  int16_t bmpHeight, uint8_t colorMode) {
	//Valid size?
	if(bmpWidth<=0||bmpHeight<=0) return bitmap_InvalidSize;
	//Anything to draw on screen?
	if (x > SSD1306_WIDTH || y > SSD1306_HEIGHT || (x + bmpWidth < 0) || (y + bmpHeight < 0)) return bitmap_DrawOffTheScreen;

	//Ignore calculations off the screen. Y dim only.
	//Above
	if(y<0){
		//Remember y is negative
		bitmap+=((bmpWidth+7)>>3)*-y;
		bmpHeight+=y;
		y=0;
	}
	//Under
	if (y + bmpHeight > SSD1306_HEIGHT) bmpHeight = SSD1306_HEIGHT - y;

	/*
	 * Borders fixed;
	 * Now prepare to write data to frame buffer
	 */
	uint8_t byte;
	uint16_t width,
			lineWidth = (bmpWidth + 7)>>3; //How many bytes is the bitmap row made of

	while(bmpHeight--) {
		width=bmpWidth;
		while(width--) {
			byte=pgm_read_byte(bitmap+(width>>3)) & (0x80 >> (width & 7));
				switch (colorMode) {
					case 0:
					case 1:
					case 2:
						if(byte) ssd1306_pixel(x + width, y, colorMode);
						break;
					default:
//					case 3:
						ssd1306_pixel(x + width, y, byte?1:0);
						break;
					case 4:
						ssd1306_pixel(x + width, y, byte?0:1);
						break;

				}
		}
		bitmap+=lineWidth;
		++y;
	}
	return ok;
}

/*
 * Draws bitmap from FLASH, XBM format.
 * Returns errorMsg
 *
 * Bytes are sorted in ROWs (not columns like frame buffer)	 *	Byte contain pixels in DESCENDING order
 *		row1: byte1 | byte2 |byte3							 *		0x80 - 1st pixel set (from left to right)
 *		row2: byte4 | byte5 |byte6							 *		0x40 - 2nd pixel set
 *		row3: ...											 *		0x01 - 8th pixel set
 *
 * ColorMode:
 * 0 - ERASEs bits masked by bitmap, blank space from bitmap is IGNORED
 * 1 - SETs bits masked by bitmap, blank space from bitmap is IGNORED
 * 2 - TOGGLEs bits masked by bitmap, blank space from bitmap is IGNORED
 *
 * 3 - OVERWRITE part of framebuffer with bitmap
 * 4 - OVERWRITE part of framebuffer with INVERTED bitmap
 * Other values are threated as 3
 */
uint8_t ssd1306_bitmapFlashXBM(int16_t x, int16_t y, const uint8_t *bitmap, int16_t bmpWidth,
					  int16_t bmpHeight, uint8_t colorMode) {
	//Valid size?
	if(bmpWidth<=0||bmpHeight<=0) return bitmap_InvalidSize;
	//Anything to draw on screen?
	if (x > SSD1306_WIDTH || y > SSD1306_HEIGHT || (x + bmpWidth < 0) || (y + bmpHeight < 0)) return bitmap_DrawOffTheScreen;

	//Ignore calculations off the screen. Y dim only.
	//Above
	if(y<0){
		//Remember y is negative
		bitmap+=((bmpWidth+7)>>3)*-y;
		bmpHeight+=y;
		y=0;
	}
	//Under
	if (y + bmpHeight > SSD1306_HEIGHT) bmpHeight = SSD1306_HEIGHT - y;

	/*
	 * Borders fixed;
	 * Now prepare to write data to frame buffer
	 */
	uint8_t byte=0;
	uint16_t width,
			lineWidth = (bmpWidth + 7)>>3; //How many bytes is the bitmap row made of

	while(bmpHeight--) {
		for (width = 0;  width < bmpWidth; ++width) {
			if(width & 7) byte >>= 1;
			else byte = pgm_read_byte(bitmap+(width>>3));

				switch (colorMode) {
					case 0:
					case 1:
					case 2:
						if(byte&1) ssd1306_pixel(x + width, y, colorMode);
						break;
					default:
//					case 3:
						ssd1306_pixel(x + width, y, byte&1?1:0);
						break;
					case 4:
						ssd1306_pixel(x + width, y, byte&1?0:1);
						break;

				}
		}
		bitmap+=lineWidth;
		++y;
	}
	return ok;
}

/*
 * Draws bitmap from RAM.
 * Returns errorMsg
 *
 * Bytes are sorted in ROWs (not columns like frame buffer)	 *	Byte contain pixels in ASCENDING order
 *		row1: byte1 | byte2 |byte3							 *		0x01 - 1st pixel set (from left to right)
 *		row2: byte4 | byte5 |byte6							 *		0x02 - 2nd pixel set
 *		row3: ...											 *		0x80 - 8th pixel set
 *
 * ColorMode:
 * 0 - ERASEs bits masked by bitmap, blank space from bitmap is IGNORED
 * 1 - SETs bits masked by bitmap, blank space from bitmap is IGNORED
 * 2 - TOGGLEs bits masked by bitmap, blank space from bitmap is IGNORED
 *
 * 3 - OVERWRITE part of framebuffer with bitmap
 * 4 - OVERWRITE part of framebuffer with INVERTED bitmap
 * Other values are threated as 3
 */
uint8_t ssd1306_bitmapRam(int16_t x, int16_t y, uint8_t *bitmap, int16_t bmpWidth,
					  int16_t bmpHeight, uint8_t colorMode) {
	//Valid size?
	if(bmpWidth<=0||bmpHeight<=0) return bitmap_InvalidSize;
	//Anything to draw on screen?
	if (x > SSD1306_WIDTH || y > SSD1306_HEIGHT || (x + bmpWidth < 0) || (y + bmpHeight < 0)) return bitmap_DrawOffTheScreen;

	//Ignore calculations off the screen. Y dim only.
	//Above
	if(y<0){
		//Remember y is negative
		bitmap+=((bmpWidth+7)>>3)*-y;
		bmpHeight+=y;
		y=0;
	}
	//Under
	if (y + bmpHeight > SSD1306_HEIGHT) bmpHeight = SSD1306_HEIGHT - y;

	/*
	 * Borders fixed;
	 * Now prepare to write data to frame buffer
	 */
	uint8_t byte;
	uint16_t width,
			lineWidth = (bmpWidth + 7)>>3; //How many bytes is the bitmap row made of

	while(bmpHeight--) {
		width=bmpWidth;
		while(width--) {
			byte=(pgm_read_byte(bitmap+(width>>3))) & (0x80 >> (width & 7));
				switch (colorMode) {
					case 0:
					case 1:
					case 2:
						if(byte) ssd1306_pixel(x + width, y, colorMode);
						break;
					default:	//case 3:
						ssd1306_pixel(x + width, y, byte?1:0);
						break;
					case 4:
						ssd1306_pixel(x + width, y, byte?0:1);
						break;
				}
		}
		bitmap+=lineWidth;
		++y;
	}
	return ok;
}

/* TODO: Create semi-universal function, with type of memory as an argument and call to specific read pixel function
 *
 * Draws bitmap from FLASH, in borderless window.
 * Returns errorMsg
 *
 * x,y defines localisation of window on screen.
 * windowHeight/Width defines height of window.
 * bmpOffset is virtual beginning of bitmap. It points where the top-left corner of window is in bitmap.
 *
 * Bytes are sorted in ROWs (not columns like frame buffer)	 *	Byte contain pixels in ASCENDING order
 *		row1: byte1 | byte2 |byte3							 *		0x01 - 1st pixel set (from left to right)
 *		row2: byte4 | byte5 |byte6							 *		0x02 - 2nd pixel set
 *		row3: ...											 *		0x80 - 8th pixel set
 *
 * ColorMode:
 * 0 - ERASEs bits masked by bitmap, blank space from bitmap is IGNORED
 * 1 - SETs bits masked by bitmap, blank space from bitmap is IGNORED
 * 2 - TOGGLEs bits masked by bitmap, blank space from bitmap is IGNORED
 *
 * 3 - OVERWRITE part of framebuffer with bitmap
 * 4 - OVERWRITE part of framebuffer with INVERTED bitmap
 * Other values are threated as 3
 */
uint8_t ssd1306_bitmapFlash_inWindow(int16_t x, int16_t y,
						  int16_t bmpOffsetX, int16_t bmpOffsetY,
						  int16_t windowWidth, int16_t windowHeight,
						  uint8_t *bitmap, int16_t bmpWidth, int16_t bmpHeight, uint8_t colorMode) {
	//Valid size?	TODO: draw and then apply mirror effect
	if(bmpWidth<=0||bmpHeight<=0) return bitmap_InvalidSize;
	if(windowHeight<=0||windowWidth<=0) return bitmap_InvalidFragmentSize;

	//Anything to draw on screen?
	if (x > SSD1306_WIDTH || y > SSD1306_HEIGHT ||								//Right and bottom
		(x + bmpOffsetX+windowWidth < 0) || (y + bmpOffsetY+windowWidth< 0))	//Left and top
			return bitmap_DrawOffTheScreen;

	/*
	 * Check if window does not exceed bitmap border.
	 * Returns are triggered only when window does not contain common pixels with bitmap.
	 */
	if(bmpOffsetX>bmpWidth||bmpOffsetY>bmpHeight)		//Starts too far right or bottom...
		return bitmap_FragmentStartsOffTheBitmap;

	if(bmpOffsetX<0){				//Window starts far too LEFT
		x-=bmpOffsetX;
		windowWidth+=bmpOffsetX;		//decrease
		bmpOffsetX=0;
	}
	if(bmpOffsetY<0){				//Window starts far too HIGH
		y-=bmpOffsetY;
		windowHeight+=bmpOffsetY;	//decrease
		bmpOffsetY=0;
	}
	if(windowHeight<=0||windowWidth<=0)				//.. or left, or top
		return bitmap_FragmentStartsOffTheBitmap;


	if(bmpOffsetX+windowWidth>bmpWidth) windowWidth=bmpWidth-bmpOffsetX;		//Window ends far too RIGHT
	if(bmpOffsetY+windowHeight>bmpHeight) windowHeight=bmpHeight-bmpOffsetY;	//Window ends far too LOW
	if(windowHeight<=0||windowWidth<=0) return bitmap_InvalidFragmentSize;

	/*
	 * Check if window does not exceed screen border.
	 * Returns are triggered only when window does not contain common pixels with screen.
	 */
	if(x+windowWidth>SSD1306_WIDTH) windowWidth=SSD1306_WIDTH-x;				//Window ends far too RIGHT to screen
	if(y+windowHeight>SSD1306_HEIGHT) windowHeight=SSD1306_HEIGHT-y;			//Window ends UNDER screen
	if(windowHeight<=0||windowWidth<=0) return bitmap_InvalidFragmentSize;


	if(x<0){				//Window starts far too LEFT to screen
		bmpOffsetX-=x;		//Remember x is negative
		windowWidth+=x;
		x=0;
	}
	if(y<0){				//Window starts ABOVE screen
		bmpOffsetY-=y;		//Remember y is negative
		windowHeight+=y;
		y=0;
	}
	if(windowHeight<=0||windowWidth<=0) return bitmap_InvalidFragmentSize;

	/*
	 * Borders fixed;
	 * Now prepare to write data to frame buffer
	 */


	uint8_t byte,
			bmpSubOffsetX=bmpOffsetX%8;		//Initial offset sets row, this variable does further offset.
	uint16_t width,
			lineWidth = (bmpWidth + 7)>>3;	//How many bytes is the bitmap row made of

	bitmap+=lineWidth*bmpOffsetY+(bmpOffsetX>>3); //Initial offsets

	while(windowHeight--) {
		width=windowWidth;
		while(width--) {
			byte=pgm_read_byte(bitmap+((bmpSubOffsetX+width)>>3)) & (0x80 >> ((bmpSubOffsetX+width) & 7));
			switch (colorMode) {
				case 0:
				case 1:
				case 2:
					if(byte) ssd1306_pixel(x + width, y, colorMode);
					break;
				default:	//case 3:
					ssd1306_pixel(x + width, y, byte?1:0);
					break;
				case 4:
					ssd1306_pixel(x + width, y, byte?0:1);
					break;
			}
		}

		bitmap+=lineWidth;
		++y;
	}
	return ok;
}

/*
 * Mirror selected area according to vertical axis
 * Returns errorMsg
 */
uint8_t ssd1306_mirrorVerticalAxis(int16_t x, int16_t y, int16_t w, int16_t h) {
	uint8_t tmp;	//used for masks and later to remember swapped byte

	//Begin calculations from top-left corner
	if(w<0)	x-=(w=-w);
	if(h<0)	y-=(h=-h);


	//Ignore calculations off the screen. X dim
	//Left
	if (x < 0) {
		x=-x;
		ssd1306_rectangleFilled(-x+w,y,x,h,0);
		w -= x;
		x = 0;
	}
	//Right
	if ((x + w) >= SSD1306_WIDTH) {
		tmp = x+w-SSD1306_WIDTH;
		ssd1306_rectangleFilled(x,y,tmp,h,0);
		x+=tmp;
		w=SSD1306_WIDTH-x;
	}

	//Ignore calculations off the screen. Y dim
	//Above
	if (y < 0) {
		//Remember y is negative
		h += y;
		y = 0;
	}
	//Under
	if ((y + h) > SSD1306_HEIGHT) {
		h=SSD1306_HEIGHT-y;
	}
	//If there is nothing to draw
	if (w <= 0 || h <= 0) return bitmap_InvalidSize;
	if(w==1) return ok;	//Or already done :P
	

	uint8_t mask,
			horizontal = w>>1,
			beginMask=y%8,
			endMask=(y+h)%8,
			lineCnt=(h+beginMask+7)>>3;
	uint8_t *ptr;
	y>>=3;	//transform y to count of pages between topmost and mottommost

	//Create masks for topmost part
	tmp=beginMask;
	beginMask=0;
	for (int8_t i = tmp; i <8; ++i) {
		beginMask+=(1<<i);
	}
	//Create masks for bottommost part
	tmp=endMask;
	endMask=0;
	for (int8_t i = tmp; i <8; ++i) {
		endMask+=(1<<i);
	}
	endMask=~endMask;

	while (horizontal--) {
		uint8_t vertical=lineCnt;
		while(vertical--){
			if (vertical==0)				//topmost
				mask=beginMask;
			else if(vertical==lineCnt-1)	//bottommost
				mask=endMask;
			else							//mid
				mask=0xFF;
			ptr=(SSD1306_frameBuff+(y+vertical)*SSD1306_WIDTH+x);	//speeds up calculations

			tmp=*(ptr+horizontal)&mask;
			*(ptr+horizontal)=(*(ptr+w-horizontal-1)&mask)+((*(ptr+horizontal))&(~mask));
			*(ptr+w-horizontal-1)=tmp+((*(ptr+w-horizontal-1))&(~mask));

		}
	}
	return ok;
}

/*
 * Mirror selected area according to horizontal axis
 * Returns errorMsg
 */
uint8_t ssd1306_mirrorHorizontalAxis(int16_t x, int16_t y, int16_t w, int16_t h) {
	uint8_t tmp1,tmp2;

	//Begin calculations from top-left corner
	if(w<0)	x-=(w=-w);
	if(h<0)	y-=(h=-h);


	//Ignore calculations off the screen. X dim
	//Left
	if (x < 0) {
		w += x;	//Remember x is negative
		x = 0;
	}
	//Right
	if ((x + w) >= SSD1306_WIDTH)
		w = (SSD1306_WIDTH - x);

	//Ignore calculations off the screen. Y dim
	//Above
	if (y < 0) {
		//Remember y is negative
		ssd1306_rectangleFilled(x,h,w,y,0);
		h += y;
		y = 0;
	}
	//Under
	if ((y + h) > SSD1306_HEIGHT) {
		tmp1= y+h-SSD1306_HEIGHT;
		ssd1306_rectangleFilled(x,y,w,tmp1,0);
		y+=tmp1;
		h=SSD1306_HEIGHT-y;
	}
	//If there is nothing to draw
	if (w <= 0 || h <= 0) return bitmap_InvalidSize;
	if(h==1) return ok;	//Or already done :P



	for (uint8_t hori = 0; hori < w; ++hori) {
		uint8_t vert=h>>1;
		while (vert--) {
			tmp1=ssd1306_getPixel(x+hori,y+vert);
			tmp2=ssd1306_getPixel(x+hori,y+h-vert-1);
			ssd1306_pixel(x+hori,y+vert,tmp2);
			ssd1306_pixel(x+hori,y+h-vert-1,tmp1);
		}
	}
	return ok;
}

