# ssd1306_OLED
My code to control I2C (and SPI, but that's not tested)  ssd1306 OLED displays. Comments are mainly written in Polish. 


TODO:
Test _drawRectangle - wychodzenie z prawej strony i od do�u
ssd1306.c


OPIS:
Biblioteka zawdzi�cza prawie wszystko Adafruit z ich GFX-Library oraz Miros�awowi Kardasiowi. Ja przeportowa�em kod z C++ na czyste C, podzieli�em go na kilka plik�w, poprawi�em par� b��d�w i postara�em si� gdzieniegdzie troszk� zoptymalizowa�.

common:
	
	- tu jest bufor obrazu w RAM,
	- r�ne definicje wsp�lne dla wszystkich plik�w
	- definicje zast�pcze gdy uruchamiamy na komputerze

ssd1306 - hardware: 	

	- do��cza graphics_mono,
	- inicjalizacja,
	- nadawanie obrazu,
	- funkcje bazuj�ce na sterowniku,
	- definicje komend sterownika,
	
graphics_mono:

	- mo�liwo�� wy��czenia poszczeg�lnych modu��w, 
	- funkcje graficzne do dzia�ania na frameBuffie,
	- front-end.
	  
fonts:

	- mo�liwo�� wy��czenia poszczeg�lnych modu��w,
	- korzysta z graphics_mono,
	- zawiera funkcje rysowania tekstu na g��wnej bitmapie (framebuff).
	Sam fakt istnienia plik�w - nawet niezaincludowanych zwi�ksza rozmiar projektu..?
	 