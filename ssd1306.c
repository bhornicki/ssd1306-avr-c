
#include "ssd1306.h"
#if !(EMULATOR_ENABLED!=0)

#include "../I2C/i2c.h"




#ifdef _SSD1306_SPI
#include <avr/io.h>
static void SPIwrite(uint8_t dat) {
	uint8_t i;
	for (i = 0x80; i; i >>= 1) {
		SCK_LO;
		if (dat & i) MOSI_HI;
		else MOSI_LO;
		SCK_HI;
	}
}
#endif

void ssd1306__cmd(uint8_t cmd) {
#ifndef _SSD1306_SPI
	i2c_write_single(SSD1306_ADDR, 0, cmd);
#else
#if USE_CS==1
	CS_HI;
#endif
	DC_LO;
#if USE_CS==1
	CS_LO;
#endif
	SPIwrite(cmd);
#if USE_CS==1
	CS_HI;
#endif
#endif
}
void ssd1306__data(uint8_t dat) {
#ifndef _SSD1306_SPI
	i2c_write_single(SSD1306_ADDR, 0x40, dat);
#else
#if USE_CS==1
	CS_HI;
#endif
	DC_HI;
#if USE_CS==1
	CS_LO;
#endif
	SPIwrite(dat);
#if USE_CS==1
	CS_HI;
#endif
#endif
}



/*
 * Wstaw za SEI, gdy korzystasz z I2C opartego na przerwaniach
 * Typowo przekazujesz (SSD1306_SWITCHCAPVCC, SSD1306_REFRESH_MID, 0)
 *
 * invertDisp:
 * 0 - standard mode
 * 1 - mirrored vertically
 * 2 - mirrored horizontally
 * 3 - rotated 180deg (mirrored in both axis)
 */
void ssd1306_init(uint8_t vcc, uint8_t refresh, uint8_t invertDisp) {

#ifdef _SSD1306_SPI
	MOSI_DDR |= MOSI;
	SCK_DDR |= SCK;
//	SCK_PORT|=SCK;

	DC_DDR |= DC;

#if USE_RST==1
	RST_DDR|=RST;
	RST_PORT|=RST;
#endif
#if USE_CS==1
	CS_DDR |= CS;
	CS_PORT |= CS;
#endif

#if USE_RST==1
	RST_HI;
	_delay_ms(25);
	RST_LO;
	_delay_ms(25);
	RST_HI;
#endif
#if USE_CS==1
	CS_HI;
	_delay_ms(25);
	CS_LO;
	_delay_ms(25);
	CS_HI;
#endif
#endif
	ssd1306__cmd(SSD1306_DISPLAYOFF);                    // 0xAE
	ssd1306__cmd(SSD1306_SETDISPLAYCLOCKDIV);            // 0xD5
	ssd1306__cmd(refresh);                            // the suggested ratio 0x80

	ssd1306__cmd(SSD1306_SETDISPLAYOFFSET);              // 0xD3
	ssd1306__cmd(0x0);                                   // no offset
	ssd1306__cmd(SSD1306_SETSTARTLINE | 0x0);            // line #0
	ssd1306__cmd(SSD1306_CHARGEPUMP);                    // 0x8D

	if (vcc == SSD1306_EXTERNALVCC) ssd1306__cmd(0x10);
	else ssd1306__cmd(0x14);

	ssd1306__cmd(SSD1306_MEMORYMODE);                    // 0x20
	ssd1306__cmd(0x00);                                  // 0x0 act like ks0108
	if (invertDisp&1)	//mirror horizontally
		ssd1306__cmd(SSD1306_SEGREMAP | 0x0);
	else
		ssd1306__cmd(SSD1306_SEGREMAP | 0x1);
	if (invertDisp&2)	//mirror vertically
		ssd1306__cmd(SSD1306_COMSCANINC);
	else
		ssd1306__cmd(SSD1306_COMSCANDEC);

	ssd1306__cmd(SSD1306_SETCONTRAST);

	if (vcc == SSD1306_EXTERNALVCC) ssd1306__cmd(0x9F);
	else ssd1306__cmd(0xCF);

	ssd1306__cmd(SSD1306_SETPRECHARGE);

	ssd1306__cmd(SSD1306_SETMULTIPLEX);
	ssd1306__cmd(0X3F);
	ssd1306__cmd(SSD1306_SETCOMPINS);
	ssd1306__cmd(0x12);
	ssd1306__cmd(SSD1306_DISPLAYALLON_RESUME);           // 0xA4
	ssd1306__cmd(SSD1306_NORMALDISPLAY);                 // 0xA6
	ssd1306__cmd(SSD1306_DEACTIVATE_SCROLL);

	ssd1306__cmd(SSD1306_DISPLAYON);
	ssd1306_display();
	/*
	 * W sumie programista jest jak Jezus
	 * Dokonuje cudu zamiany, tylko nie wody w wino,
	 * ale kawy w kod. I Jezus te� nie mia� �r�d�a
	 * �eby naprawi� �wiat.
	 *
	 * Dlatego wierz w Odyna. Jezus obieca� po�o�y� kres z�ym ludziom
	 * A Odyn lodowym olbrzymom.
	 * Widzia�e� gdzie� w okolicy lodowe olbrzymy?
	 */
}

/*
 * Dzia�a tylko w trybie I2C ! ! !
 *
 * Wy�wietla poziomy rz�d o wysoko�ci 8
 * Wy�wietlacz sk�ada si� z 8 takich rz�d�w, numerownanych od 0 do 7
 * Gdy pozycja startowa jest wi�ksza od ko�cowej nic nie jest wy�wietlane.
 * Gdy chcesz wy�wietli� jedn� lini�, start = stop.
 */
void ssd1306_displayLine(uint8_t start, uint8_t stop) {
	if (stop > 7) stop = 7;
	if (start > stop) return;
	ssd1306__cmd(SSD1306_SETLOWCOLUMN | 0x0);
	ssd1306__cmd(SSD1306_SETHIGHCOLUMN | 0x0);
	ssd1306__cmd(SSD1306_SETSTARTLINE | 0x0);
	ssd1306__cmd(SSD1306_PAGEADDR);
	ssd1306__cmd(start);
	ssd1306__cmd(stop);

#ifndef _SSD1306_SPI
	i2c_write_buf(SSD1306_ADDR, 0x40, (stop - start + 1) << 7, SSD1306_frameBuff + (start << 7) );
#else
#if USE_CS==1
	CS_HI;
#endif
	DC_HI;
#if USE_CS==1
	CS_LO;
#endif
	uint8_t *ptr = SSD1306_frameBuff+(start << 7);
	for (uint16_t i = 0; i < (stop - start + 1) << 7; i++)
	SPIwrite(SSD1306_frameBuff++);
#if USE_CS==1
	CS_HI;
#endif
#endif

}

/*
 * Wysy�a ca�y bufor do wy�wietlacza.
 * Wyt�umaczy mi kto�, dlaczego metoda z define (patrz i2c) zajmuje wi�cej miejsca?
 */
void ssd1306_display(void) {
	ssd1306_displayLine(0, 7);
}

#endif
